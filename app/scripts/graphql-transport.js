/*
  Simple GraphQL transport which send queries to a GraphQL endpoint
*/
const GraphQLTransport = function(path) {
  this.path = path || "/graphql";
}

GraphQLTransport.prototype.sendQuery = function (query, variables) {
  var self = this;
  variables = variables || {};
  return new Promise(function(resolve, reject) {
    // use fetch to get the result
    fetch(self.path, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        query,
        variables
      })
    })
    // get the result as JSON
    .then(function(res) {
      return res.json();
    })
    // trigger result or reject
    .then(function(response) {
      if(response.errors) {
        return reject(response.errors);
      }

      return resolve(response.data);
    })
    .catch(reject);
  });
};
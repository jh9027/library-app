(function () {
  'use strict';

  const transport = new GraphQLTransport("http://localhost:3000/graphql");

  var viewModel = new ViewModel(transport);
  ko.applyBindings(viewModel);
  viewModel.fetchBooks();

})();
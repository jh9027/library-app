// The query required to fetch data for this view
const query = `query BookList(
    $genre: String,
    $authorGender: String,
    $publishedDate: String,
    $sort: String,
    $sortDirection: String,
    $count: Int,
    $offset: Int
  ) {
    books(
        genre: $genre,
        authorGender: $authorGender,
        publishedDate: $publishedDate,
        sort: $sort,
        sortDirection: $sortDirection,
        count: $count,
        offset: $offset
    ) {
      title
      genre
      published
      author {
        name
        gender
      }
    }
  }`;

// the primary view model
const ViewModel = function (transport) {

  this._transport = transport;

  // Array of books to display
  this.books = ko.observableArray();

  // Sort by field
  this.sort = ko.observable();

  // Sort order
  this.sortDirection = ko.observable("asc");

  // Author gender filter
  this.gender = ko.observable();

  // Genre filter
  this.genre = ko.observable();

  // Keep track of the offset when loading 'infinite' scrolling data
  this.offset = ko.observable(0);

  // Boolean to indicate if data is being loaded
  this.dataLoading = ko.observable(true);

  // Boolean indicating if there is more data to load
  this.moreData = ko.observable(true);

  // A place to store error messages, on update they'll be displayed to the user
  this.errorMessage = ko.observable();
};

/*
 * Fetch a list of books from the transport layer, as per the defined query
 *
 * @param vars Object of arguments to send with the query. Defaults to the
 *             value returned from getFilterData().
 * @param append Boolean indicating if the new data should be appended to the
                 existing data or replace it. Defaults to false.
 */
ViewModel.prototype.fetchBooks = function(
    { vars=this.getFilterData(), append=false } = {}) {

  // hide any previous error messages
  this.discardErrors();

  if (!append) {
    this.books([]); // Remove existing books
    this.offset(0); // Reset the offset
    this.moreData(true); // Reset the 'moreData' status
  } else {
    this.offset(this.offset() + this._booksPerReq); // Increment the offset
  }

  // Set loading indicator to true
  this.dataLoading(true);

  // Send query for books
  var booksPromise = this._transport.sendQuery(query,
    { offset: this.offset(), count: this._booksPerReq, ...vars });

  booksPromise.then(res => {
    // If there's no more data to load (empty dataset returned)
    if (!res.books) {
      this.moreData(false);
    }

    // Append/replace the data
    if (!append) {
      this.books(res.books);
    } else {
      ko.utils.arrayPushAll(this.books, res.books);
    }

    // Set loading indicator to false
    this.dataLoading(false);
  }, reason => { // Handle rejection
    this.errorMessage(`Ooops, an error occurred whilst trying to load data from the server.
      Check the console for more details.`);
    console.log(reason);
    this.dataLoading(false);
  });
};

// Returns an object of form values keyed ready to use with the graphQL query
ViewModel.prototype.getFilterData = function() {
  return {
    sort: this.sort(),
    sortDirection: this.sortDirection(),
    authorGender: this.gender(),
    genre: this.genre()
  };
};

// Capture scroll events and load more data if necessary
ViewModel.prototype.scrollEvent = function(data, ev) {
  // If we're already loading data or if there's no more data to load don't do anything
  if (this.dataLoading() || !this.moreData()) {
    return;
  }
  var el = ev.target;
  if (el.scrollTop > (el.scrollHeight - el.offsetHeight - 600)) {
    this.fetchBooks({ append: true });
  }
};

ViewModel.prototype.discardErrors = function() {
  this.errorMessage(null);
}

// Options for the gender dropdown
ViewModel.prototype._genderOptions = ko.observableArray([
  {title: "Male", value: "Male"},
  {title: "Female", value: "female"}
]);

// Options for the genre dropdown
ViewModel.prototype._genreOptions = ko.observableArray([
  'Horror',
  'Science fiction',
  'Satire',
  'Drama',
  'Action and Adventure',
  'Romance',
  'Mystery',
  'Finance'
]);

// Options for the sort field dropdown
ViewModel.prototype._sortOptions = ko.observableArray([
  {title: "Genre", value: "genre"},
  {title: "Title", value: "title"},
  {title: "Published Date", value: "published"},
  {title: "Author Name", value: "author.name"},
  {title: "Author Gender", value: "author.gender"}
]);

// The number of books to fetch on each trip to the server
ViewModel.prototype._booksPerReq = 1000;
# Library App Readme

This is a simple front-end for the [library-app-server](https://bitbucket.org/jh9027/library-app-server) that includes features such as infinite scrolling, filtering and sorting.

## Technologies

- Yeoman webapp generator was used to setup the initial application structure
- Gulp task runner
- KnockoutJS handles updates to the UI
- Bulma CSS framework (http://bulma.io/). Chosen because it's fairly lightweight but still allowed me to write very little CSS.
- Font Awesome

## Prerequisites

The following is a rough list of prerequisites. You may have success with alternate versions but this is what has been tested:

- Node v4 or higher
- npm 3.10 or higher
- The following npm packages:
  - gulp-cli
  - bower

## Usage

1. Run `npm install && bower install`
2. Run `gulp serve` to run the development server
3. Ensure the library-app-server is [library-app-server](https://bitbucket.org/jh9027/library-app-server). This should be on the same machine as the client; if not update the URL in `app/scripts/main.js`
4. Access the app at [http://localhost:9000/](http://localhost:9000/)

## Assumptions, Limitations & Comments

- The error handling on the ajax requests is still fairly basic. In a real world application I would consider using automatic retries with an exponential backoff.
- The two preset buttons aren't currently implemented although I have made a start on the code for generating a list of last Friday dates in the library-app-server repository.
- The table scrolling isn't great, given more time I would setup fixed headers or something similar to make it more user friendly.
- The are **NO TESTS**, again as a result of time constraints. This isn't designed to be a production ready app so correctness was sacrificed in favour of features :)
- `fetchBooks` should probably be moved out of the `ViewModel` and into a seperate data layer. I haven't done this simply because of the size of the application - it's quite maintainable as it is at the moment and there are no plans to add extra data requrements.
- The usage of GraphQL probably isn't optimal. I wanted to experiment but the learning curve was steeper than I expected! The main area I'd like to revisit is how the author fields currently use arguments on the books object - this causes a significant performance hit when sorting on these fields.